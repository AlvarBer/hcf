using UnityEngine;

namespace HCF {
	[ExecuteAlways]
	public class SetCameraDepthNormalTex : MonoBehaviour {
		private void OnEnable() => Camera.main.depthTextureMode = DepthTextureMode.DepthNormals;
		private void OnDisable() => Camera.main.depthTextureMode = DepthTextureMode.None;
	}
}