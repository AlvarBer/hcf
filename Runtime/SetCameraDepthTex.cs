using UnityEngine;

namespace HCF {
	[ExecuteAlways]
	public class SetCameraDepthTex : MonoBehaviour {
		private void OnEnable() => Camera.main.depthTextureMode = DepthTextureMode.Depth;
		private void OnDisable() => Camera.main.depthTextureMode = DepthTextureMode.None;
	}
}