﻿using System;
using UnityEngine;

namespace HCF {
	[AttributeUsage(AttributeTargets.Field)]
	public class AutohookAttribute : PropertyAttribute { }
}