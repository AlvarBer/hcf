using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HCF {
	public static class Extensions {
		/// <summary>
		/// Swaps two values using references
		/// </summary>
		public static T Swap<T>(this T x, ref T y) {
			T temp = y;
			y = x;
			return temp;
		}

		// Random functions
		/// <summary>
		/// Returns a random true or false
		/// </summary>
		public static bool RandomBool(this MonoBehaviour r) => UnityEngine.Random.value > 0.5;

		/// <summary>
		/// Invokes a coroutine after duration
		/// </summary>
		public static void RunAfter(this MonoBehaviour m, float duration, Action f) {
			IEnumerator RunAfter_(Action action, float dur) {
				yield return new WaitForSeconds(dur);
				action();
			}
			m.StartCoroutine(RunAfter_(f, duration));
		}

		/// <summary>
		/// Returns the required drag to a certain speed with an acceleration
		/// <param name="acceleration">Acceleration (in m/s^2)</param>
		/// <param name="terminalSpeed">Max speed (in m/s)</param>
		/// </summary>
		// https://answers.unity.com/questions/819273/force-to-velocity-scaling.html?childToView=819474#answer-819474
		public static float DragRequiredFromAccel(this Rigidbody2D r, float acceleration, float terminalSpeed) {
			return DragRequiredFromImpulse(r, acceleration * Time.fixedDeltaTime, terminalSpeed);
		}

		public static float DragRequiredFromImpulse(this Rigidbody2D r, float speed, float terminalSpeed) {
			return speed / ((terminalSpeed + speed) * Time.fixedDeltaTime) / r.mass;
		}

		/// <summary>
		/// Remaps a value from a range to another
		/// </summary>
		public static float Remap(this float value, float from1, float to1, float from2, float to2) {
			return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
		}

		/// <summary>
		/// Returns true if bounding contains bounded
		/// </summary>
		public static bool Contains(this Bounds bounding, Bounds bounded) {
			return bounding.Contains(bounded.min) && bounding.Contains(bounded.max);
		}

		/// <summary>
		/// Returns upper left corner
		/// </summary>
		public static Vector2 LeftUpCorner(this Bounds bounds) {
			return new Vector2(bounds.min.x, bounds.max.y);
		}

		/// <summary>
		/// Returns true down right corner
		/// </summary>
		public static Vector2 DownRightCorner(this Bounds bounds) {
			return new Vector2(bounds.max.x, bounds.min.y);
		}

		/// <summary>
		/// Given an angle in degrees returns the vector
		/// </summary>
		public static Vector2 AngleToDirection(this float angle) {
			float angleRad = angle * Mathf.Deg2Rad;
			return new Vector2(Mathf.Cos(angleRad), Mathf.Sin(angleRad));
		}

		/// <summary>
		/// Given a normal vector returns the angle in degrees
		/// </summary>
		public static float DirectionToAngle(this Vector2 v) => Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;

		/// <summary>
		/// Returns a point given by four corners
		/// <param name="xStart">X dimension start</param>
		/// <param name="xFinish">X dimension finish</param>
		/// <param name="yStart">Y dimension start</param>
		/// <param name="yFinish">Y dimension finish</param>
		/// </summary>
		public static Vector2 RandomCorners(
			this MonoBehaviour m,
			float xStart,
			float xFinish,
			float yStart,
			float yFinish
		) {
			float x = UnityEngine.Random.Range(xStart, xFinish);
			float y = UnityEngine.Random.Range(yStart, yFinish);
			Vector2 point = new Vector2(x, y);
			float xSign = Mathf.Pow(-1, UnityEngine.Random.Range(1, 3));
			float ySign = Mathf.Pow(-1, UnityEngine.Random.Range(1, 3));
			Vector2 signs = new Vector2(xSign, ySign);
			return Vector2.Scale(point, signs);
		}

		public static Matrix4x4 Rows(
			this Matrix4x4 matrix,
			Vector4 row0,
			Vector4 row1,
			Vector4 row2,
			Vector4 row3
		) {
			matrix.SetRow(0, row0);
			matrix.SetRow(1, row1);
			matrix.SetRow(2, row2);
			matrix.SetRow(3, row3);
			return matrix;
		}

		public static Matrix4x4 Identity(this Matrix4x4 matrix) {
			matrix.SetRow(0, new Vector4(1, 0, 0, 0));
			matrix.SetRow(1, new Vector4(0, 1, 0, 0));
			matrix.SetRow(2, new Vector4(0, 0, 1, 0));
			matrix.SetRow(3, new Vector4(0, 0, 0, 1));
			return matrix;
		}

		public static void ReloadCurrentScene(this MonoBehaviour m) => SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
}