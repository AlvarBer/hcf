using UnityEngine;

namespace HCF {
	[ExecuteAlways]
	public class Blit : MonoBehaviour {
		[SerializeField]
		private Material mat = default;

		private void OnRenderImage(RenderTexture src, RenderTexture dst) {
			if (mat != null) {
				Graphics.Blit(src, dst, mat);
			} else {
				Graphics.Blit(src, dst);
			}
		}
	}
}