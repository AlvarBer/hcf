﻿using HCF;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace HCF {
	public class ObjectPool<T, U> : Singleton<U>
		where T : MonoBehaviour
		where U : MonoBehaviour {
		public T prefab;
		public int size;
		public event Action OnPoolFull;
		// All operations should remain O(1)
		private List<T> freeObjects;
		private List<T> usedObjects;

		protected override void Awake() {
			base.Awake();
			freeObjects = new List<T>(size);
			usedObjects = new List<T>(size);

			for (var i = 0; i < size; i++) {
				T pooledObject = Instantiate(prefab, this.transform);
				pooledObject.gameObject.SetActive(false);
				freeObjects.Add(pooledObject);
			}
		}

		public T Instantiate() {
			if (freeObjects.Count == 0) {
				return null;
			}
			T pooledObject = freeObjects[freeObjects.Count - 1];
			freeObjects.RemoveAt(freeObjects.Count - 1);
			usedObjects.Add(pooledObject);
			return pooledObject;
		}

		public T Instantiate(Vector3 position, Quaternion rotation) {
			T pooledObject = Instantiate();
			if (pooledObject == null) {
				return null;
			}
			pooledObject.transform.position = position;
			pooledObject.transform.rotation = rotation;
			pooledObject.gameObject.SetActive(true);
			return pooledObject;
		}

		public void Destroy(T item) {
			if (freeObjects.Contains(item)) {
				Debug.Log($"{item} has already been reclaimed");
				return;
			}
			Debug.Assert(usedObjects.Contains(item), "Object doesn't belong in the pool");
			Debug.Assert(freeObjects.Count < freeObjects.Capacity, "Trying to destroy a pooled object twice");
			// This is O(n), should swap with last first
			usedObjects.Remove(item);
			item.gameObject.SetActive(false);
			freeObjects.Add(item);
			if (freeObjects.Count == freeObjects.Capacity) {
				OnPoolFull?.Invoke();
			}
		}

		public void Clear() {
			for (int i = 0; i < usedObjects.Count; i++) {
				usedObjects[i].gameObject.SetActive(false);
				freeObjects.Add(usedObjects[i]);
			}
			usedObjects.Clear();
		}
	}
}