﻿using UnityEngine;

namespace HCF {
	public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour {
		public static T instance => _instance;
		private static T _instance;

		protected virtual void Awake() {
			if (_instance == null) {
				_instance = this as T;
			} else if (instance != this) {
				Debug.Log("Singleton already present", this);
				Destroy(this.gameObject);
			}
		}
	}

}
