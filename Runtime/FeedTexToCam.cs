using UnityEngine;

[ExecuteAlways]
public class FeedTexToCam : MonoBehaviour {
	[SerializeField]
	private RenderTexture renderTex = null;

	private void OnEnable() => Camera.main.targetTexture = renderTex;

	private void OnDisable() => Camera.main.targetTexture = null;

	private void Start() => GUI.depth = 20;

	private void OnGUI() {
		if (Camera.main?.targetTexture != null) {
			GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), Camera.main.activeTexture);
		}
	}
}
