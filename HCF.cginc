#if !defined(HCF_INCLUDED)
#define HCF_INCLUDED

// Simple noise function, sourced from http://answers.unity.com/answers/624136/view.html
// Extended discussion on this function can be found at the following link:
// https://forum.unity.com/threads/am-i-over-complicating-this-random-function.454887/#post-2949326
// Returns a number in the 0...1 range.
float rand(float3 co) {
	return frac(sin(dot(co.xyz, float3(12.9898, 78.233, 53.539))) * 43758.5453);
}

float3 random3(float3 c) {
	float j = 4096 * sin(dot(c, float3(17, 59.4, 15)));
	float3 r;
	r.z = frac(512 * j);
	j *= 0.125;
	r.x = frac(512 * j);
	j *= 0.125;
	r.y = frac(512 * j);
	return r - 0.5;
}

// Ripped from shadergraph
// Permutation and hashing used in webgl-noise goo.gl/pX7HtC
float2 gradientNoiseDirFloat(float2 p) {
	p = p % 289;
	float x = (34 * p.x + 1) * p.x % 289 + p.y;
	x = (34 * x + 1) * x % 289;
	x = frac(x / 41) * 2 - 1;
	return normalize(float2(x - floor(x + 0.5), abs(x) - 0.5));
}

float gradientNoise(float2 uv, float scale) {
	float2 p = uv * scale;
	float2 ip = floor(p);
	float2 fp = frac(p);
	float d00 = dot(gradientNoiseDirFloat(ip), fp);
	float d01 = dot(gradientNoiseDirFloat(ip + float2(0, 1)), fp - float2(0, 1));
	float d10 = dot(gradientNoiseDirFloat(ip + float2(1, 0)), fp - float2(1, 0));
	float d11 = dot(gradientNoiseDirFloat(ip + float2(1, 1)), fp - float2(1, 1));
	fp = fp * fp * fp * (fp * (fp * 6 - 15) + 10);
	return lerp(lerp(d00, d01, fp.y), lerp(d10, d11, fp.y), fp.x) + 0.5;
}

float remap(float val, float2 inRange, float2 outRange) {
	return outRange.x + (val - inRange.x) * (outRange.y - outRange.x) / (inRange.y - inRange.x);
}

float remapTo01(float val, float2 inRange) {
	return (val - inRange.x) / (inRange.y - inRange.x);
}

float remapFrom01(float val, float2 outRange) {
	return outRange.x + val * (outRange.y - outRange.x);
}

// Ripped from shadergraph contrast node
float3 contrast(float3 In, float contrast) {
	float midpoint = pow(0.5, 2.2);
	return (In - midpoint) * contrast + midpoint;
}

// Ripped from shadergraph voronoi node
inline float2 Unity_Voronoi_RandomVector_float(float2 UV, float offset) {
	float2x2 m = float2x2(15.27, 47.63, 99.41, 89.98);
	UV = frac(sin(mul(UV, m)) * 46839.32);
	return float2(sin(UV.y*+offset)*0.5+0.5, cos(UV.x*offset)*0.5+0.5);
}

float voronoi(float2 UV, float AngleOffset, float CellDensity) {
	float2 g = floor(UV * CellDensity);
	float2 f = frac(UV * CellDensity);
	float t = 8.0;
	float3 res = float3(8.0, 0.0, 0.0);
	float result;
	float Cells;

	for (int y=-1; y<=1; y++) {
		for (int x=-1; x<=1; x++) {
			float2 lattice = float2(x,y);
			float2 offset = Unity_Voronoi_RandomVector_float(lattice + g, AngleOffset);
			float d = distance(lattice + offset, f);

			if (d < res.x) {
				res = float3(d, offset.x, offset.y);
				result = res.x;
				Cells = res.y;
			}
		}
	}

	return result;
}

// Ripped from shader graph radial shear node
float2 radialShear(float2 UV, float2 center, float2 strength, float2 offset) {
	float2 delta = UV - center;
	float delta2 = dot(delta.xy, delta.xy);
	float2 delta_offset = delta2 * strength;
	return UV + float2(delta.y, -delta.x) * delta_offset + offset;
}

// https://chicounity3d.wordpress.com/2014/05/23/how-to-lerp-like-a-pro/
float smootherstep(float x) {
	return x * x * x * (x * (6 * x - 15) + 10);
}

// Triangle wave between 0 and 1
float trig(float x) {
	return (0.5 - abs(x - 0.5)) * 2;
}

// A softer triangle that uses the square wave
float waveTrig(float x) {
	return -pow(x * 2 - 1, 2) + 1;
}

// Ripped from shadergraph
float4 blendBurn(float4 base, float4 blend, float opacity) {
	float4 result = 1 - (1 - blend) / base;
	return lerp(base, result, opacity);
}
#endif