HCF
===

Halt and Catch Fire is a Unity package with a random assorment of useful
things.

install via adding `"com.alvarber.hcf": "https://gitlab.com/AlvarBer/hcf.git#0.0.11",`
to your `Packages/manifest.json`.

Remember to put `using HCF;` in every file you want to use HCF features.

Features
--------
* Singleton
* Object Pool
* Autohook
* MaxMinSlider (Currently broken)
* Simple blit script
* Set camera depth or depth and normal textures
* Useful extension such as
  * Random bool.
  * Run `Action` after delay
  * Drag required to reach x speed with y acceleration.
  * Remap from range (x, y) to (z, w).
  * Check if `Bounds` is contained inside another `Bounds`.
  * Given an angle in degrees return the normal vector & viceversa.
* Useful shader functions via `#include "Packages/HCF/HCF.cginc"` such as
  * `random` & `random3`
  * `trig` trinagle wave & `waveTrig` square wave
  * `smootherstep`.
  * Various shadergraph functions like
    * `gradientNoise`
    * `remap`, `remapTo01` & `remapFrom01`
    * `voronoi` noise.