using System.IO;
using UnityEditor;
using UnityEngine;


public class GradientMaker : EditorWindow
{
    private Gradient gradient = new Gradient();
    private Vector2Int gradientTextureSize = new Vector2Int(128, 1);

    private Texture2D texture2D;
    private string lastFileName = "gradient";
    private string lastDirectory = "";

    private void Awake()
    {
        lastDirectory = Application.dataPath;
    }

    [MenuItem("Tools/Gradient Maker")]
    public static void ShowWindow() => GetWindow<GradientMaker>("Gradient Maker");

    private void OnGUI()
    {
        GUILayout.Label("Gradient Maker", EditorStyles.boldLabel);

        gradient = EditorGUILayout.GradientField("Gradient", gradient);
        gradientTextureSize = EditorGUILayout.Vector2IntField("Gradient size", gradientTextureSize);

        texture2D = new Texture2D(gradientTextureSize.x, gradientTextureSize.y);
        texture2D.wrapMode = TextureWrapMode.Clamp;
        for (int x = 0; x < gradientTextureSize.x; x++)
        {
            Color col = gradient.Evaluate((float)x / (float)gradientTextureSize.x);
            for (int y = 0; y < gradientTextureSize.y; y++)
            {
                texture2D.SetPixel(x, y, col);
            }
        }
        texture2D.Apply();

        if (GUILayout.Button("Save gradient"))
        {
            SaveTexture();
        }
    }

    private void SaveTexture()
    {
        string path = EditorUtility.SaveFilePanel(
            "Save Texture as PNG",
            lastDirectory,
            lastFileName + ".png",
            "png"
        );

        if (path != "")
        {
            byte[] bytes = texture2D.EncodeToPNG();
            File.WriteAllBytes(path, bytes);
            AssetDatabase.Refresh();
            Debug.Log($"Gradient saved at {path}");
            lastDirectory = Path.GetDirectoryName(path);
            lastFileName = Path.GetFileNameWithoutExtension(path);
        }
    }
}
