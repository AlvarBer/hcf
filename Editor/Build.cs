using UnityEditor;
using UnityEngine;
using System;
using static System.Environment;
using System.IO;
using System.Linq;

namespace HCF {
	static class Build {
		[MenuItem("Tools/Build/Win IL2CPP")]
		public static void BuildWinIL2CPP() {
			PlayerSettings.SetScriptingBackend(BuildTargetGroup.Standalone, ScriptingImplementation.IL2CPP);
			BuildPlayer($"Builds/Win/{PlayerSettings.productName}.exe", BuildTarget.StandaloneWindows64);
			Directory.Delete($"Builds/Win/{PlayerSettings.productName}_BurstDebugInformation_DoNotShip", true);
			File.Delete("Builds/Win/UnityCrashHandler64.exe");
		}

		[MenuItem("Tools/Build/Win Mono")]
		public static void BuildWin() {
			PlayerSettings.SetScriptingBackend(BuildTargetGroup.Standalone, ScriptingImplementation.Mono2x);
			BuildPlayer($"Builds/Win/{PlayerSettings.productName}.exe", BuildTarget.StandaloneWindows64);
			Directory.Delete($"Builds/Win/{PlayerSettings.productName}_BurstDebugInformation_DoNotShip", true);
			File.Delete("Builds/Win/UnityCrashHandler64.exe");
		}

		[MenuItem("Tools/Build/Web")]
		public static void BuildHtml() {
			BuildPlayer($"Builds/Html/", BuildTarget.WebGL);
		}

		[MenuItem("Tools/Build/Linux mono")]
		public static void BuildLinux() {
			PlayerSettings.SetScriptingBackend(BuildTargetGroup.Standalone, ScriptingImplementation.Mono2x);
			BuildPlayer($"Builds/Linux/{PlayerSettings.productName}", BuildTarget.StandaloneLinux64);
			File.Delete("Builds/Linux/LinuxPlayer_s.debug");
			File.Delete("Builds/Linux/UnityPlayer_s.debug");
			Directory.Delete($"Builds/Linux/{PlayerSettings.productName}_BurstDebugInformation_DoNotShip", true);
		}

		[MenuItem("Tools/Build/OS X mono")]
		public static void BuildOSX() {
			PlayerSettings.SetScriptingBackend(BuildTargetGroup.Standalone, ScriptingImplementation.Mono2x);
			BuildPlayer($"Builds/OSX/{PlayerSettings.productName}", BuildTarget.StandaloneOSX);
			Directory.Delete($"Builds/OSX/{PlayerSettings.productName}_BurstDebugInformation_DoNotShip", true);
		}

		public static void BuildPlayer(string path, BuildTarget target) {
			Debug.Log($"Building for {target}");
			Directory.CreateDirectory(Path.GetDirectoryName(path));
			Debug.Log($"Created directory {Path.GetDirectoryName(path)}");
			BuildPlayerOptions options = new BuildPlayerOptions {
				locationPathName = path,
				scenes = GetEnabledScenes(),
				options = BuildOptions.StrictMode,
				target = target,
			};
			PlayerSettings.bundleVersion = GetArgument("buildNumber") ?? PlayerSettings.bundleVersion;
			Debug.Log($"About to build player with options {options}");
			BuildPipeline.BuildPlayer(options);
			Debug.Log($"Build for {target }done");
		}

		private static string GetArgument(string name) {
			string[] args = GetCommandLineArgs();
			for (int i = 0; i < args.Length; i++) {
				if (args[i].Contains(name)) {
					return args[i + 1];
				}
			}
			return null;
		}

		private static string[] GetEnabledScenes() {
			return EditorBuildSettings.scenes
				.Where(scene => scene.enabled && !string.IsNullOrEmpty(scene.path))
				.Select(scene => scene.path)
				.ToArray();
		}
	}
}
