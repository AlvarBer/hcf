using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

public class ListVertexColors : EditorWindow {
	[MenuItem("CONTEXT/MeshFilter/List Colors")]
	static void ListColors(MenuCommand command) {
		MeshFilter meshFilter = command.context as MeshFilter;
		List<Color> colors = new List<Color>();
		meshFilter?.sharedMesh?.GetColors(colors);
		foreach (var color in colors?.Distinct()) {
			Debug.Log(ColorUtility.ToHtmlStringRGBA(color));
		}
	}

	[MenuItem("CONTEXT/MeshFilter/List Colors", true)]
	static bool ListColorsValidate() => HasMeshFilter(Selection.activeGameObject);

	[MenuItem("CONTEXT/MeshFilter/List Vertices")]
	static void ListVertices(MenuCommand command) {
		MeshFilter meshFilter = command.context as MeshFilter;
		Debug.Log($"Num of vertices: {meshFilter?.sharedMesh?.vertexCount}");
	}

	[MenuItem("CONTEXT/MeshFilter/See Normals")]
	static void SeeNormals(MenuCommand command) {
		MeshFilter meshFilter = command.context as MeshFilter;
		Mesh mesh = meshFilter.sharedMesh;
		foreach (int i in Enumerable.Range(0, mesh.vertexCount)) {
			Debug.DrawRay(mesh.vertices[i], mesh.normals[i]);
		}
	}

	static bool HasMeshFilter(GameObject go) => go.GetComponent<MeshFilter>() != null;
}
